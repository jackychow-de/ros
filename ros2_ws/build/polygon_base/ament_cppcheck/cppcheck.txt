-- run_test.py: invoking following command in '/home/jacky/ros2_ws/src/polygon_base':
 - /opt/ros/humble/bin/ament_cppcheck --xunit-file /home/jacky/ros2_ws/build/polygon_base/test_results/polygon_base/cppcheck.xunit.xml --include_dirs /home/jacky/ros2_ws/src/polygon_base/include

cppcheck 2.7 has known performance issues and therefore will not be used, set the AMENT_CPPCHECK_ALLOW_SLOW_VERSIONS environment variable to override this.


-- run_test.py: return code 0
-- run_test.py: verify result file '/home/jacky/ros2_ws/build/polygon_base/test_results/polygon_base/cppcheck.xunit.xml'
