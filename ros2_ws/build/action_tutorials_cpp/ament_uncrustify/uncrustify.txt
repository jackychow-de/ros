-- run_test.py: invoking following command in '/home/jacky/ros2_ws/src/action_tutorials_cpp':
 - /opt/ros/humble/bin/ament_uncrustify --xunit-file /home/jacky/ros2_ws/build/action_tutorials_cpp/test_results/action_tutorials_cpp/uncrustify.xunit.xml

Code style divergence in file 'include/action_tutorials_cpp/visibility_control.h':

--- include/action_tutorials_cpp/visibility_control.h
+++ include/action_tutorials_cpp/visibility_control.h.uncrustify
@@ -44 +44 @@
-#endif  // ACTION_TUTORIALS_CPP__VISIBILITY_CONTROL_H_
+#endif  // ACTION_TUTORIALS_CPP__VISIBILITY_CONTROL_H_

Code style divergence in file 'src/fibonacci_action_client.cpp':

--- src/fibonacci_action_client.cpp
+++ src/fibonacci_action_client.cpp.uncrustify
@@ -111 +111 @@
-RCLCPP_COMPONENTS_REGISTER_NODE(action_tutorials_cpp::FibonacciActionClient)
+RCLCPP_COMPONENTS_REGISTER_NODE(action_tutorials_cpp::FibonacciActionClient)

Code style divergence in file 'src/fibonacci_action_server.cpp':

--- src/fibonacci_action_server.cpp
+++ src/fibonacci_action_server.cpp.uncrustify
@@ -117 +117 @@
-RCLCPP_COMPONENTS_REGISTER_NODE(action_tutorials_cpp::FibonacciActionServer)
+RCLCPP_COMPONENTS_REGISTER_NODE(action_tutorials_cpp::FibonacciActionServer)

3 files with code style divergence


-- run_test.py: return code 1
-- run_test.py: verify result file '/home/jacky/ros2_ws/build/action_tutorials_cpp/test_results/action_tutorials_cpp/uncrustify.xunit.xml'
