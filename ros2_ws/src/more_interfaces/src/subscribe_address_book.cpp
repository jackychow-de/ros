#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "more_interfaces/msg/address_book.hpp"

using std::placeholders::_1;

class AddressBookSubscriber: public rclcpp::Node
{
public:
  AddressBookSubscriber()
  : Node("address_book_subscriber")
  {
    subscription_ = this->create_subscription<more_interfaces::msg::AddressBook>(    // CHANGE
      "address_book", 10, std::bind(&AddressBookSubscriber::topic_callback, this, _1));
  }

private:
  void topic_callback(const more_interfaces::msg::AddressBook & msg) const  // CHANGE
  {
    RCLCPP_INFO_STREAM(this->get_logger(), "I heard: '" << msg.first_name << "'");  
    RCLCPP_INFO_STREAM(this->get_logger(), "I heard: '" << msg.last_name << "'");  
    RCLCPP_INFO_STREAM(this->get_logger(), "I heard: '" << msg.phone_number  << "'");  
    RCLCPP_INFO_STREAM(this->get_logger(), "I heard: '" << msg.PHONE_TYPE_MOBILE << "'");  
  }
  rclcpp::Subscription<more_interfaces::msg::AddressBook>::SharedPtr subscription_;  // CHANGE
};


int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<AddressBookSubscriber>());
  rclcpp::shutdown();
  return 0;
}