import os
import launch
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from webots_ros2_driver.webots_launcher import WebotsLauncher
from webots_ros2_driver.webots_controller import WebotsController
#from rclpy.node import Node
from launch_ros.actions import Node


def generate_launch_description():
    package_dir = get_package_share_directory('my_package_wb')
    robot_description_path = os.path.join(package_dir, 'resource', 'my_robot.urdf')

    #Specify in the constructor which world file the simulator will open
    webots = WebotsLauncher(
        world=os.path.join(package_dir, 'worlds', 'my_world.wbt')
    )
    #Wb controller located in the webots_ros2_driver package.
    my_robot_driver = WebotsController(
        robot_name='my_robot',
        parameters=[
            {'robot_description': robot_description_path},
        ]
    )
    
    obstacle_avoider = Node(
        package='my_package_wb',
        executable='obstacle_avoider',  
        #the path to the URDF file which refers to the MyRobotDriver plugin
    )

    return LaunchDescription([
        webots,
        my_robot_driver,
        obstacle_avoider,
        launch.actions.RegisterEventHandler(
            event_handler=launch.event_handlers.OnProcessExit(
                target_action=webots,
                on_exit=[launch.actions.EmitEvent(event=launch.events.Shutdown())],
            )
        )
    ])