# CMake generated Testfile for 
# Source directory: /home/jacky/ros2_test_ws/src/test_cpp
# Build directory: /home/jacky/ros2_test_ws/build/test_cpp
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_cpp_test "/usr/bin/python3.10" "-u" "/opt/ros/humble/share/ament_cmake_test/cmake/run_test.py" "/home/jacky/ros2_test_ws/build/test_cpp/test_results/test_cpp/test_cpp_test.gtest.xml" "--package-name" "test_cpp" "--output-file" "/home/jacky/ros2_test_ws/build/test_cpp/ament_cmake_gtest/test_cpp_test.txt" "--command" "/home/jacky/ros2_test_ws/build/test_cpp/test_cpp_test" "--gtest_output=xml:/home/jacky/ros2_test_ws/build/test_cpp/test_results/test_cpp/test_cpp_test.gtest.xml")
set_tests_properties(test_cpp_test PROPERTIES  LABELS "gtest" REQUIRED_FILES "/home/jacky/ros2_test_ws/build/test_cpp/test_cpp_test" TIMEOUT "60" WORKING_DIRECTORY "/home/jacky/ros2_test_ws/build/test_cpp" _BACKTRACE_TRIPLES "/opt/ros/humble/share/ament_cmake_test/cmake/ament_add_test.cmake;125;add_test;/opt/ros/humble/share/ament_cmake_gtest/cmake/ament_add_gtest_test.cmake;86;ament_add_test;/opt/ros/humble/share/ament_cmake_gtest/cmake/ament_add_gtest.cmake;93;ament_add_gtest_test;/home/jacky/ros2_test_ws/src/test_cpp/CMakeLists.txt;46;ament_add_gtest;/home/jacky/ros2_test_ws/src/test_cpp/CMakeLists.txt;0;")
subdirs("gtest")
