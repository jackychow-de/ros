<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

<meta name="google-site-verification" content="CjkdY6BqKWAVmQ78_iSq6J7ZZ9AoL7-CjFVBYGg9FU4" />
<link rel="shortcut icon" href="/custom/favicon.ico" type="image/ico" />
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" />
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/custom/js/sorttable.js"></script>
<script type="text/javascript" src="/custom/libraries/jquery.min.js"></script>                         
<script type="text/javascript" src="/custom/js/seesaw.js"></script> 
<script type="text/javascript" src="/custom/js/rosversion.js"></script> 
<!--script type="text/javascript" src="/custom/libraries/RGraph.common.core.js" ></script Used for metrics only, should be conditional, not at the top-->
<!--script type="text/javascript" src="/custom/libraries/RGraph.bar.js" ></script same as above -->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17821189-2']);
  _gaq.push(['_setDomainName', 'wiki.ros.org']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="robots" content="index,nofollow">

<title>rosdistro/Tutorials/Indexing Your ROS Repository for Documentation Generation - ROS Wiki</title>
<script type="text/javascript" src="/moin_static197/common/js/common.js"></script>

<script type="text/javascript">
<!--
var search_hint = "Search";
//-->
</script>


<link rel="stylesheet" type="text/css" charset="utf-8" media="all" href="/moin_static197/rostheme/css/common.css">
<link rel="stylesheet" type="text/css" charset="utf-8" media="screen" href="/moin_static197/rostheme/css/screen.css">
<link rel="stylesheet" type="text/css" charset="utf-8" media="print" href="/moin_static197/rostheme/css/print.css">
<link rel="stylesheet" type="text/css" charset="utf-8" media="projection" href="/moin_static197/rostheme/css/projection.css">

<!-- css only for MS IE6/IE7 browsers -->
<!--[if lt IE 8]>
   <link rel="stylesheet" type="text/css" charset="utf-8" media="all" href="/moin_static197/rostheme/css/msie.css">
<![endif]-->




<link rel="canonical" href="http://wiki.ros.org/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation" />

<link rel="Start" href="/Documentation">
<link rel="Alternate" title="Wiki Markup" href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=raw">
<link rel="Alternate" media="print" title="Print View" href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=print">
<link rel="Up" href="/rosdistro/Tutorials">
<link rel="Appendix" title="rosdistro_fork.png" href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=view&amp;target=rosdistro_fork.png">
<link rel="Appendix" title="rosdistro_pull.png" href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=view&amp;target=rosdistro_pull.png">
<link rel="Appendix" title="rosdistro_pull_submit.png" href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=view&amp;target=rosdistro_pull_submit.png">
<link rel="Appendix" title="wiki.ros.org_packageHeader.png" href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=view&amp;target=wiki.ros.org_packageHeader.png">
<link rel="Search" href="/FindPage">
<link rel="Index" href="/TitleIndex">
<link rel="Glossary" href="/WordIndex">
<link rel="Help" href="/HelpOnFormatting">
</head>

<body  lang="en" dir="ltr">

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-EVD5Z6G6NH"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-EVD5Z6G6NH');
</script>

<script type="text/javascript">
<!--// Initialize search form
var f = document.getElementById('searchform');
if(f) f.getElementsByTagName('label')[0].style.display = 'none';
var e = document.getElementById('searchinput');
if(e) {
  searchChange(e);
  searchBlur(e);
}

function handleSubmit() {
  var f = document.getElementById('searchform');
  var t = document.getElementById('searchinput');
  var r = document.getElementById('real_searchinput');

  //alert("handleSubmit "+ t.value);
  if(t.value.match(/review/)) {
    r.value = t.value;
  } else {
    //r.value = t.value + " -PackageReviewCategory -StackReviewCategory -M3Review -DocReview -ApiReview -HelpOn -BadContent -LocalSpellingWords";
    r.value = t.value + " -PackageReviewCategory -StackReviewCategory -DocReview -ApiReview";
  }
  //return validate(f);
}
//-->
</script>

<div id="dpage">
  <div id="dpage-inner">
    <div id="header"><div id="topnav">

<!-- Alert box -->
<!-- To enable/disable the alert box, uncomment/comment the block below and update the content below the button. -->
<!--
    <div class="alert alert-info alert-dismissable" style="text-align: center;">
<script>
jQuery(function( $ ){
    alert_box_name = 'roswiki_roscon'
    state = localStorage.getItem(alert_box_name);
    if (!state) {
      localStorage.setItem(alert_box_name, 'open');
    }
    state = localStorage.getItem(alert_box_name);
    if (state == 'closed') {
        $( '#topnav .alert' ).hide();
    }
    $( '.close' ).click(function () {
	console.log('in .close.click');
        localStorage.setItem(alert_box_name, 'closed');
        $( '#topnav .alert' ).hide();
  });
});
</script>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <a href="http://roscon.ros.org">ROSCon 2017</a> will be in Vancouver, September 21-22! <br>
      The last few years have sold out: register now and take advantage of discounted hotel rates</a>.
    </div>
-->
<!-- End Alert box -->

      <table id="topnav-table">
        <tr>
          <td width="300" valign="top"><a href="/"><img border="0" src="/custom/images/ros_org.png" alt="ros.org" width="238" height="51"/></a></td>
          <td width="500" valign="middle">
            <a href="http://www.ros.org/about-ros">About</a>
            |
            <a href="/Support">Support</a>
            |
            <a href="http://discourse.ros.org/">Discussion Forum</a>
            |
            <a href="http://index.ros.org/">Index</a>
            |
            <a href="http://status.ros.org/">Service Status</a>
            |
            <a href="http://answers.ros.org/">Q&A answers.ros.org</a>
          </td>

      </tr>
      <tr>
        <td colspan="3" height="53" width="1024"><nobr><img src="/custom/images/menu_left.png" width="17" height="53" alt=""/><a href="/"><img
        border="0" src="/custom/images/menu_documentation.png" width="237" height="53" alt="Documentation" /></a><img
        src="/custom/images/menu_spacer.png" width="69" height="53" /><a href="https://index.ros.org/packages"><img border="0"
        src="/custom/images/menu_browse_software.png" width="268" height="53" alt="Browse Software" /></a><img
        src="/custom/images/menu_spacer.png" width="69" height="53" /><a href="https://discourse.ros.org/c/general"><img border="0"
        src="/custom/images/menu_news.png" width="84" height="53" alt="News" /></a><img
        src="/custom/images/menu_spacer.png" width="69" height="53" /><a href="/ROS/Installation"><img border="0"
        src="/custom/images/menu_download.png" width="151" height="53" alt="Download" /></a><img
        src="/custom/images/menu_right.png" width="60" height="53" /></nobr></td>

      </tr>
    </table> <!-- topnav-table -->

    </div> <!-- /#topav -->
  </div> <!-- /#header -->


<div id="header2">

<ul id="pagelocation">
<li><a href="/rosdistro">rosdistro</a></li><li><a href="/rosdistro/Tutorials">Tutorials</a></li><li><a class="backlink" href="/action/fullsearch/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=fullsearch&amp;context=180&amp;value=linkto%3A%22rosdistro%2FTutorials%2FIndexing+Your+ROS+Repository+for+Documentation+Generation%22" rel="nofollow" title="Click to do a full-text search for this title">Indexing Your ROS Repository for Documentation Generation</a></li>
</ul>

<div id="locationline">
</div>
</div>

<div id="sidebar">
<h4>ROS 2 Documentation</h4><p>The ROS Wiki is for ROS 1. Are you using ROS 2 (<a href="http://docs.ros.org/en/humble/">Humble</a>, <a href="http://docs.ros.org/en/iron/">Iron</a>, or <a href="http://docs.ros.org/en/rolling/">Rolling</a>)? <br><a href="http://docs.ros.org">Check out the ROS 2 Project Documentation</a><br>Package specific documentation can be found on <a href="https://index.ros.org">index.ros.org</a></p>
<div class="sidepanel">
<h1>Wiki</h1>

<ul id="navibar">
<li class="wikilink"><a href="/Distributions">Distributions</a></li><li class="wikilink"><a href="/ROS/Installation">ROS/Installation</a></li><li class="wikilink"><a href="/ROS/Tutorials">ROS/Tutorials</a></li><li class="wikilink"><a href="/RecentChanges">RecentChanges</a></li><li class="current"><a href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation">Indexing Yo... Generation</a></li>
</ul>

</div>
<div class="sidepanel">
<h1>Page</h1>
<ul class="editbar"><li><span class="disabled">Immutable Page</span></li><li class="toggleCommentsButton" style="display:none;"><a href="#" class="nbcomment" onClick="toggleComments();return false;">Comments</a></li><li><a class="nbinfo" href="/action/info/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=info" rel="nofollow">Info</a></li><li><a class="nbattachments" href="/action/AttachFile/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile" rel="nofollow">Attachments</a></li><li>
<form class="actionsmenu" method="GET" action="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation">
<div>
    <label>More Actions:</label>
    <select name="action"
        onchange="if ((this.selectedIndex != 0) &&
                      (this.options[this.selectedIndex].disabled == false)) {
                this.form.submit();
            }
            this.selectedIndex = 0;">
        <option value="raw">Raw Text</option>
<option value="print">Print View</option>
<option value="RenderAsDocbook">Render as Docbook</option>
<option value="show" disabled class="disabled">Delete Cache</option>
<option value="show" disabled class="disabled">------------------------</option>
<option value="SpellCheck">Check Spelling</option>
<option value="LikePages">Like Pages</option>
<option value="LocalSiteMap">Local Site Map</option>
<option value="show" disabled class="disabled">------------------------</option>
<option value="RenamePage" disabled class="disabled">Rename Page</option>
<option value="CopyPage">Copy Page</option>
<option value="DeletePage" disabled class="disabled">Delete Page</option>
<option value="show" disabled class="disabled">------------------------</option>
<option value="MyPages">My Pages</option>
<option value="show" disabled class="disabled">Subscribe User</option>
<option value="show" disabled class="disabled">------------------------</option>
<option value="show" disabled class="disabled">Remove Spam</option>
<option value="show" disabled class="disabled">Revert to this revision</option>
<option value="PackagePages">Package Pages</option>
<option value="SyncPages">Sync Pages</option>
<option value="show" disabled class="disabled">------------------------</option>
<option value="CreatePdfDocument">CreatePdfDocument</option>
<option value="Load">Load</option>
<option value="RawFile">RawFile</option>
<option value="Save">Save</option>
<option value="SlideShow">SlideShow</option>
    </select>
    <input type="submit" value="Do">
    
</div>
<script type="text/javascript">
<!--// Init menu
actionsMenuInit('More Actions:');
//-->
</script>
</form>
</li></ul>

</div>
<div class="sidepanel">
<h1>User</h1>
<ul id="username"><li><a href="/action/login/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=login" id="login" rel="nofollow">Login</a></li></ul>
</div>
</div>

<div id="page" lang="en" dir="ltr">

<div dir="ltr" id="content" lang="en"><span class="anchor" id="top"></span>
<span class="anchor" id="line-1"></span><span class="anchor" id="line-2"></span><span class="anchor" id="line-3"></span><span class="anchor" id="line-4"></span><span class="anchor" id="line-5"></span><span class="anchor" id="line-6"></span><span class="anchor" id="line-7"></span><span class="anchor" id="line-8"></span><span class="anchor" id="line-9"></span><span class="anchor" id="line-10"></span><span class="anchor" id="line-11"></span><span class="anchor" id="line-12"></span><span class="anchor" id="line-13"></span><span class="anchor" id="line-14"></span><span class="anchor" id="line-15"></span><span class="anchor" id="line-16"></span><span class="anchor" id="line-17"></span><span class="anchor" id="line-18"></span><span class="anchor" id="line-19"></span><span class="anchor" id="line-20"></span><span class="anchor" id="line-21"></span><span class="anchor" id="line-22"></span><span class="anchor" id="line-23"></span><span class="anchor" id="line-24"></span><p class="line867"><span class="anchor" id="line-1-1"></span><span class="anchor" id="line-2-1"></span><div><table><tbody><tr>  <td style="background-color: #cce0ff"><strong>Note:</strong> First create your own ROS repository using the <a href="/RecommendedRepositoryUsage">RecommendedRepositoryUsage</a> page. This tutorial assumes that you have completed the previous tutorials:   Once you've created your own ROS package repository, you can document your stacks and packages on ROS.org.. </td>
</tr>
</tbody></table></div><span class="anchor" id="line-3-1"></span><span class="anchor" id="line-4-1"></span><span class="anchor" id="line-5-1"></span><span class="anchor" id="line-6-1"></span><div><table><tbody><tr>  <td style="background-color: #bbceee"> <img alt="(!)" height="15" src="/moin_static197/rostheme/img/idea.png" title="(!)" width="15" /> Please ask about problems and questions regarding this tutorial on <a class="http" href="http://answers.ros.org">answers.ros.org</a>. Don't forget to include in your question the link to this page, the versions of your OS &amp; ROS, and also add appropriate tags.</td>
</tr>
</tbody></table></div><span class="anchor" id="line-7-1"></span><span class="anchor" id="line-8-1"></span><span class="anchor" id="line-9-1"></span>
<h1 id="Indexing_Your_ROS_Repository_for_Documentation_Generation">Indexing Your ROS Repository for Documentation Generation</h1>
<span class="anchor" id="line-10-1"></span><strong>Description:</strong> There are hundreds of ROS repositories hosted by companies, universities, and individuals around the world. Whether you're releasing one ROS package or hundreds, feel free to start your own repository to share your code with the rest of the community.<br>
<br>
 <span class="anchor" id="line-11-1"></span><span class="anchor" id="line-12-1"></span><span class="anchor" id="line-13-1"></span><span class="anchor" id="line-14-1"></span><strong>Tutorial Level:</strong>  INTERMEDIATE <br>
 <br>
 <span class="anchor" id="line-15-1"></span><span class="anchor" id="line-16-1"></span><span class="anchor" id="line-17-1"></span><p class="line874"> <span class="anchor" id="line-25"></span><span class="anchor" id="line-26"></span><p class="line867"><span class="anchor" id="line-27"></span><span class="anchor" id="line-28"></span><div class="yellow"><span class="anchor" id="line-1-2"></span><p class="line862">This is a tutorial for indexing for documentation only. To make a release into binary builds see the <a href="/bloom">bloom</a> documentation. </div><span class="anchor" id="line-29"></span><p class="line867"><div class="table-of-contents"><p class="table-of-contents-heading">Contents<ol><li>
<a href="#Indexing_Your_ROS_package_Repository_for_Documentation_Generation">Indexing Your ROS package Repository for Documentation Generation</a><ol><li>
<a href="#What_you_get_by_adding_your_package_to_the_indexer">What you get by adding your package to the indexer</a></li><li>
<a href="#First_time_for_a_package_to_be_added_to_the_indexer">First time for a package to be added to the indexer</a><ol><li>
<a href="#Steps_to_have_your_repository_added_to_our_indexer">Steps to have your repository added to our indexer</a></li><li>
<a href="#Create_a_wiki_page_for_your_package_in_very_short_steps">Create a wiki page for your package in very short steps</a></li></ol></li><li>
<a href="#Pull_in_changes_from_upstream">Pull in changes from upstream</a></li></ol></li><li>
<a href="#Check_for_the_indexer_to_successfully_index_your_ROS_package">Check for the indexer to successfully index your ROS package</a></li><li>
<a href="#Documenting_Your_ROS_package_Repository_on_ROS.org">Documenting Your ROS package Repository on ROS.org</a></li><li>
<a href="#Announce_your_package">Announce your package</a><ol><li>
<a href="#Template_for_a_new_package_announcement">Template for a new package announcement</a></li></ol></li></ol></div><p class="line874"> <span class="anchor" id="line-30"></span><span class="anchor" id="line-31"></span><p class="line867">
<h1 id="Indexing_Your_ROS_package_Repository_for_Documentation_Generation">Indexing Your ROS package Repository for Documentation Generation</h1>
<span class="anchor" id="line-32"></span><p class="line874">Once you've created your own ROS package repository, you can document your stacks and packages on ROS.org. <span class="anchor" id="line-33"></span><span class="anchor" id="line-34"></span><p class="line867">
<h2 id="What_you_get_by_adding_your_package_to_the_indexer">What you get by adding your package to the indexer</h2>
<span class="anchor" id="line-35"></span><p class="line867"><img alt="wiki.ros.org_packageHeader.png" class="attachment" src="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=get&amp;target=wiki.ros.org_packageHeader.png" title="wiki.ros.org_packageHeader.png" width="75%" /> <span class="anchor" id="line-36"></span><span class="anchor" id="line-37"></span><p class="line862">(this image is editable <a class="https" href="https://docs.google.com/drawings/d/1aa8tFOi0L-v_dRYQlvrNc7w1SBFeosHQJRzHl3xoZ94/edit?usp=sharing">here</a>.) <span class="anchor" id="line-38"></span><span class="anchor" id="line-39"></span><ul><li><p class="line862">Your package's wiki page will automatically pull in information from <tt class="backtick">package.xml</tt> in your package (see the image above). <span class="anchor" id="line-40"></span></li><li>(A very good) Side effect; Pulled information helps internet search engines indexing your page, leads to better visibility. <span class="anchor" id="line-41"></span><span class="anchor" id="line-42"></span></li></ul><p class="line867">
<h2 id="First_time_for_a_package_to_be_added_to_the_indexer">First time for a package to be added to the indexer</h2>
<span class="anchor" id="line-43"></span><p class="line867">
<h3 id="Steps_to_have_your_repository_added_to_our_indexer">Steps to have your repository added to our indexer</h3>
<span class="anchor" id="line-44"></span><ul><li><p class="line862">First, fork the following repository on GitHub <a class="https" href="https://github.com/ros/rosdistro">https://github.com/ros/rosdistro</a> <img alt="rosdistro_fork.png" class="attachment" src="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=get&amp;target=rosdistro_fork.png" title="rosdistro_fork.png" /> <span class="anchor" id="line-45"></span><ul><li><p class="line862">More information on forking GitHub repositories: <a class="https" href="https://help.github.com/articles/fork-a-repo">https://help.github.com/articles/fork-a-repo</a> <span class="anchor" id="line-46"></span></li></ul></li><li>Checkout your forked copy of the repository for editing <span class="anchor" id="line-47"></span><ul><li><p class="line891"><tt>git&nbsp;clone&nbsp;git@github.com:my_github_name/rosdistro.git</tt> <span class="anchor" id="line-48"></span></li></ul></li><li>In your copy of the rosdistro repository browse to the distribution directory you'd like to update. <span class="anchor" id="line-49"></span><ul><li><p class="line891"><tt>cd&nbsp;rosdistro/&lt;DISTRO_NAME&gt;</tt> <span class="anchor" id="line-50"></span></li></ul></li><li><p class="line862">Inside of this directory, edit the distribution.yaml file and add your repository information (while maintaining alphabetic order of the repositories) with a <tt class="backtick">doc</tt> section. <span class="anchor" id="line-51"></span><ul><li>The repository url should point to the upstream repository, not the release repository. <span class="anchor" id="line-52"></span></li><li><p class="line862">Optionally, you repository entry can contain a <tt class="backtick">depends</tt> value which contains a list of other repository names listed in this file upon which your code (and it's documentation) depend, but which you do not want to document explicitly in the job associated with your repository. <span class="anchor" id="line-53"></span></li></ul></li><li>Run the following script in the root of your rosdistro checkout to make your file comply to rosdistros special formatting rules. It will change the files in place, so if you want to be extra safe you could back up your changes before. <span class="anchor" id="line-54"></span><ul><li><p class="line891"><tt>rosdistro_reformat&nbsp;file://"$(pwd)"/index.yaml</tt> <span class="anchor" id="line-55"></span></li></ul></li><li>Verify your changes before commiting the changes <span class="anchor" id="line-56"></span><ul><li><p class="line891"><tt>git&nbsp;diff&nbsp;distribution.yaml</tt> <span class="anchor" id="line-57"></span></li><li><p class="line891"><tt>git&nbsp;commit&nbsp;-m&nbsp;"Adding&nbsp;my_repo_name&nbsp;to&nbsp;documentation&nbsp;index&nbsp;for&nbsp;distro"&nbsp;distribution.yaml</tt> <span class="anchor" id="line-58"></span></li></ul></li><li><p class="line862">Push your changes to your remote GitHub repository <tt>git&nbsp;push&nbsp;origin&nbsp;master</tt> <span class="anchor" id="line-59"></span></li><li><p class="line862">Submit a pull request on GitHub to the rosdistro project to add your rosinstall files to the indexer. <img alt="rosdistro_pull.png" class="attachment" src="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=get&amp;target=rosdistro_pull.png" title="rosdistro_pull.png" /> <img alt="rosdistro_pull_submit.png" class="attachment" src="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation?action=AttachFile&amp;do=get&amp;target=rosdistro_pull_submit.png" title="rosdistro_pull_submit.png" /> <span class="anchor" id="line-60"></span><ul><li><p class="line862">Concrete examples of a pull request: <a class="https" href="https://github.com/ros/rosdistro/pull/13688/files">1</a>, <a class="https" href="https://github.com/ros/rosdistro/pull/13665/files">2</a> <span class="anchor" id="line-61"></span><span class="anchor" id="line-62"></span></li></ul></li></ul><p class="line867">
<h3 id="Create_a_wiki_page_for_your_package_in_very_short_steps">Create a wiki page for your package in very short steps</h3>
<span class="anchor" id="line-63"></span><ol type="1"><li>Create a wiki.ros.org account if necessary and log on. <span class="anchor" id="line-64"></span></li><li>Enter into the wiki the url of the page you want to create. <span class="anchor" id="line-65"></span><ul><li><p class="line862">e.g. "<tt class="backtick">http://wiki.ros.org/your_package</tt>" for "your_package". <span class="anchor" id="line-66"></span></li></ul></li><li>The page opened above will say the page does not exist, select the appropriate Template from the list to create your page. <span class="anchor" id="line-67"></span><ul><li><p class="line862">For a package, choose "<tt class="backtick">PackageTemplate</tt>". <span class="anchor" id="line-68"></span></li></ul></li><li><p class="line862">We also encourage you to create usage documentation, tutorials, reviews, or whatever other content you want. See the <a href="/PackageDocumentation">guide to writing package documentation</a> <span class="anchor" id="line-69"></span><span class="anchor" id="line-70"></span></li></ol><p class="line874">That's it! Once your pull request is serviced, documentation for your rosinstall files will be generated the next time the documentation jobs run. <span class="anchor" id="line-71"></span><span class="anchor" id="line-72"></span><p class="line867">
<h2 id="Pull_in_changes_from_upstream">Pull in changes from upstream</h2>
<span class="anchor" id="line-73"></span><p class="line874">To pull in changes from github.com/ros/rosdistro, you need to add another remote <span class="anchor" id="line-74"></span><span class="anchor" id="line-75"></span><p class="line867"><tt>git&nbsp;remote&nbsp;add&nbsp;upstream&nbsp;https://github.com/ros/rosdistro.git</tt> <span class="anchor" id="line-76"></span><span class="anchor" id="line-77"></span><p class="line874">Now to pull changes other people have made and ROS has commited to github: <span class="anchor" id="line-78"></span><span class="anchor" id="line-79"></span><p class="line867"><tt>git&nbsp;pull&nbsp;upstream</tt> <span class="anchor" id="line-80"></span><span class="anchor" id="line-81"></span><p class="line862">Git will pull changes from upstream and automatically merge them into your current branch. This single step could won't notify you of conflicts between your local repo and upstream. You may have to identify a branch when using this: <tt>git&nbsp;pull&nbsp;upstream&nbsp;master</tt> <span class="anchor" id="line-82"></span><span class="anchor" id="line-83"></span><p class="line874">An alternative is a two step process: <span class="anchor" id="line-84"></span><span class="anchor" id="line-85"></span><p class="line867"><tt>git&nbsp;fetch&nbsp;upstream</tt> <span class="anchor" id="line-86"></span><span class="anchor" id="line-87"></span><p class="line867"><tt>git&nbsp;merge&nbsp;upstream/master</tt> <span class="anchor" id="line-88"></span><span class="anchor" id="line-89"></span><p class="line874">This two step process will prompt you if there are any conflicts between your local repo and upstream. <span class="anchor" id="line-90"></span><span class="anchor" id="line-91"></span><p class="line867">
<h1 id="Check_for_the_indexer_to_successfully_index_your_ROS_package">Check for the indexer to successfully index your ROS package</h1>
<span class="anchor" id="line-92"></span><p class="line862">For every meta/package/stack you add an document job is created on the Jenkins server: <a class="http" href="http://build.ros.org/">http://build.ros.org/</a> (this might take a while after adding) <span class="anchor" id="line-93"></span><span class="anchor" id="line-94"></span><p class="line874">Simply enter the meta/package/stack name in the search box to find corresponding jobs. On the job page the status, failure message and full console output can be inspected. <span class="anchor" id="line-95"></span><span class="anchor" id="line-96"></span><p class="line867">
<h1 id="Documenting_Your_ROS_package_Repository_on_ROS.org">Documenting Your ROS package Repository on ROS.org</h1>
<span class="anchor" id="line-97"></span><p class="line874">Once your repository has been indexed, overnight the documentation jobs are recreated and run. <span class="anchor" id="line-98"></span><span class="anchor" id="line-99"></span><p class="line874">Creating a wiki page for your package is already explained above. To add documentation there, the following guidelines help you get started with documenting on the wiki: <span class="anchor" id="line-100"></span><span class="anchor" id="line-101"></span><ul><li><p class="line891"><a href="/StyleGuide">Wiki Style Guide</a> <span class="anchor" id="line-102"></span></li><li><p class="line891"><a href="/PackageDocumentation">Guide to Writing Package Documentation</a> <span class="anchor" id="line-103"></span></li><li><p class="line891"><a href="/WritingTutorials">Guide to Writing Tutorials</a> <span class="anchor" id="line-104"></span></li><li><p class="line891"><a href="/WikiMacros">Guide to ROS.org Wiki Macros</a> <span class="anchor" id="line-105"></span><span class="anchor" id="line-106"></span></li></ul><p class="line874">Some exemplars of good wiki documentation: <span class="anchor" id="line-107"></span><span class="anchor" id="line-108"></span><ul><li><p class="line891"><a href="/tf">tf</a> <span class="anchor" id="line-109"></span></li><li><p class="line891"><a href="/carrot_planner">carrot_planner</a> <span class="anchor" id="line-110"></span></li><li><p class="line891"><a href="/actionlib">actionlib</a> <span class="anchor" id="line-111"></span></li><li><p class="line891"><a href="/ground_station">ground_station</a> <span class="anchor" id="line-112"></span></li><li><p class="line891"><a href="/velodyne_driver">velodyne_driver</a> <span class="anchor" id="line-113"></span><span class="anchor" id="line-114"></span></li></ul><p class="line867">
<h1 id="Announce_your_package">Announce your package</h1>
<span class="anchor" id="line-115"></span><p class="line862">Being indexed on <tt class="backtick">ros.org</tt>, your package has far more visibility than ever before among the global community. Some people may find your package through Google or <a class="http" href="http://www.ros.org/browse/list.php">ROS wiki's package search</a>. It's always better, however, to let people know your work (see also <a class="https" href="https://discourse.ros.org/t/new-software-release-announcements/1203">a discussion about how announcement helps</a>). <span class="anchor" id="line-116"></span><span class="anchor" id="line-117"></span><p class="line862">To do so simply make a new post on <a class="http" href="http://discourse.ros.org/c/general">"`general`" category on discourse.ros.org</a>. <span class="anchor" id="line-118"></span><span class="anchor" id="line-119"></span><p class="line874">If you have a notable update for a package that is already released, announcement for it is also appreciated. <span class="anchor" id="line-120"></span><span class="anchor" id="line-121"></span><p class="line867">
<h2 id="Template_for_a_new_package_announcement">Template for a new package announcement</h2>
<span class="anchor" id="line-122"></span><p class="line874">If you wonder what to include in the announcement, you could add: <span class="anchor" id="line-123"></span><span class="anchor" id="line-124"></span><ul><li>Package name. <span class="anchor" id="line-125"></span></li><li>Brief description of the package (this can be a copy from your package.xml. Don't forget to add description in your package.xml everyone btw!). <span class="anchor" id="line-126"></span></li><li><p class="line862">Link to the package page on wiki.ros.org (explained <a href="/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation#Create_a_wiki_page_for_your_package_in_very_short_steps">earlier in this same wiki</a>). <span class="anchor" id="line-127"></span><ul><li>Link to the repository works too, although creating wiki page is strongly encouraged. <span class="anchor" id="line-128"></span></li></ul></li><li>Link to the tutorial of the package if exists. <span class="anchor" id="line-129"></span></li><li>Acknowledgement for the funding, notable contributers if anything. <span class="anchor" id="line-130"></span><span class="anchor" id="line-131"></span></li></ul><p class="line862">You can find <a class="https" href="https://www.google.com/search?client=ubuntu&amp;channel=fs&amp;q=site:lists.ros.org+(announce+OR+announcement+OR+announcing)&amp;ie=utf-8&amp;oe=utf-8">many previous concrete examples</a>. <span class="anchor" id="line-132"></span><span class="anchor" id="line-133"></span><p class="line867"><span class="anchor" id="line-134"></span><p class="line867"><span class="anchor" id="line-135"></span><p class="line867"><span class="anchor" id="line-136"></span><span class="anchor" id="bottom"></span></div></div>
<div id="pagebottom"></div>
</div>

<p id="pageinfo" class="info" lang="en" dir="ltr">Wiki: rosdistro/Tutorials/Indexing Your ROS Repository for Documentation Generation  (last edited 2018-08-01 07:12:54 by <span title="VictorLamoine @ ARennes-257-1-8-150.w2-11.abo.wanadoo.fr[2.11.23.150]"><a class="nonexistent" href="/VictorLamoine" title="VictorLamoine @ ARennes-257-1-8-150.w2-11.abo.wanadoo.fr[2.11.23.150]">VictorLamoine</a></span>)</p>




<div style="margin-top: 9px;" class="footer">
Except where otherwise noted, the ROS wiki is licensed under the <br /><a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0</a>
<hr style="margin-top: 10px;">
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<a href="https://www.openrobotics.org/"><img style="margin-top: -1px;" src="/custom/images/brought_by_horiz.png"></p></a>
  </div>
</div>
</div>

  </div></div> <!-- /#dpage-inner, /#dpage -->


</body>
</html>

UM���      eO��eO��F9c�eO��   �    O^partitionKey=%28http%2Cros.org%29,:http://wiki.ros.org/rosdistro/Tutorials/Indexing%20Your%20ROS%20Repository%20for%20Documentation%20Generation necko:classified 1 strongly-framed 1 request-method GET request-Cookie 7GDxgrZaVa6iV6r0PR2Xp6bSwdc= request-User-Agent Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0 request-Accept-Language en-US,en;q=0.5 response-head HTTP/1.1 200 OK
Date: Sat, 11 Nov 2023 20:28:33 GMT
Server: Apache
Vary: Cookie,User-Agent,Accept-Language
Content-Length: 34108
Content-Type: text/html; charset=utf-8
 original-response-headers Date: Sat, 11 Nov 2023 20:28:33 GMT
Server: Apache
Vary: Cookie,User-Agent,Accept-Language
Content-Length: 34108
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: text/html; charset=utf-8
 ctid 1 uncompressed-len 0 net-response-time-onstart 1159 net-response-time-onstop 1340   �<