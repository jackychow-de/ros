try {
  var session = window.sessionStorage || {};
} catch (e) {
  var session = {};
}

window.addEventListener("DOMContentLoaded", () => {
  const allTabs = document.querySelectorAll('.sphinx-tabs-tab');
  const tabLists = document.querySelectorAll('[role="tablist"]');

  allTabs.forEach(tab => {
    tab.addEventListener("click", changeTabs);
  });

  tabLists.forEach(tabList => {
    tabList.addEventListener("keydown", keyTabs);
  });

  // Restore group tab selection from session
  const lastSelected = session.getItem('sphinx-tabs-last-selected');
  if (lastSelected != null) selectNamedTabs(lastSelected);
});

/**
 * Key focus left and right between sibling elements using arrows
 * @param  {Node} e the element in focus when key was pressed
 */
function keyTabs(e) {
    const tab = e.target;
    let nextTab = null;
    if (e.keyCode === 39 || e.keyCode === 37) {
      tab.setAttribute("tabindex", -1);
      // Move right
      if (e.keyCode === 39) {
        nextTab = tab.nextElementSibling;
        if (nextTab === null) {
          nextTab = tab.parentNode.firstElementChild;
        }
      // Move left
      } else if (e.keyCode === 37) {
        nextTab = tab.previousElementSibling;
        if (nextTab === null) {
          nextTab = tab.parentNode.lastElementChild;
        }
      }
    }

    if (nextTab !== null) {
      nextTab.setAttribute("tabindex", 0);
      nextTab.focus();
    }
}

/**
 * Select or deselect clicked tab. If a group tab
 * is selected, also select tab in other tabLists.
 * @param  {Node} e the element that was clicked
 */
function changeTabs(e) {
  // Use this instead of the element that was clicked, in case it's a child
  const notSelected = this.getAttribute("aria-selected") === "false";
  const positionBefore = this.parentNode.getBoundingClientRect().top;
  const notClosable = !this.parentNode.classList.contains("closeable");

  deselectTabList(this);

  if (notSelected || notClosable) {
    selectTab(this);
    const name = this.getAttribute("name");
    selectNamedTabs(name, this.id);

    if (this.classList.contains("group-tab")) {
      // Persist during session
      session.setItem('sphinx-tabs-last-selected', name);
    }
  }

  const positionAfter = this.parentNode.getBoundingClientRect().top;
  const positionDelta = positionAfter - positionBefore;
  // Scroll to offset content resizing
  window.scrollTo(0, window.scrollY + positionDelta);
}

/**
 * Select tab and show associated panel.
 * @param  {Node} tab tab to select
 */
function selectTab(tab) {
  tab.setAttribute("aria-selected", true);

  // Show the associated panel
  document
    .getElementById(tab.getAttribute("aria-controls"))
    .removeAttribute("hidden");
}

/**
 * Hide the panels associated with all tabs within the
 * tablist containing this tab.
 * @param  {Node} tab a tab within the tablist to deselect
 */
function deselectTabList(tab) {
  const parent = tab.parentNode;
  const grandparent = parent.parentNode;

  Array.from(parent.children)
  .forEach(t => t.setAttribute("aria-selected", false));

  Array.from(grandparent.children)
    .slice(1)  // Skip tablist
    .forEach(panel => panel.setAttribute("hidden", true));
}

/**
 * Select grouped tabs with the same name, but no the tab
 * with the given id.
 * @param  {Node} name name of grouped tab to be selected
 * @param  {Node} clickedId id of clicked tab
 */
function selectNamedTabs(name, clickedId=null) {
  const groupedTabs = document.querySelectorAll(`.sphinx-tabs-tab[name="${name}"]`);
  const tabLists = Array.from(groupedTabs).map(tab => tab.parentNode);

  tabLists
    .forEach(tabList => {
      // Don't want to change the tabList containing the clicked tab
      const clickedTab = tabList.querySelector(`[id="${clickedId}"]`);
      if (clickedTab === null ) {
        // Select first tab with matching name
        const tab = tabList.querySelector(`.sphinx-tabs-tab[name="${name}"]`);
        deselectTabList(tab);
        selectTab(tab);
      }
    })
}

if (typeof exports === 'undefined') {
  exports = {};
}

exports.keyTabs = keyTabs;
exports.changeTabs = changeTabs;
exports.selectTab = selectTab;
exports.deselectTabList = deselectTabList;
exports.selectNamedTabs = selectNamedTabs;
����      eM�eM�F8��eN�   T    O^partitionKey=%28https%2Cros.org%29,:https://docs.ros.org/en/humble/_static/tabs.js strongly-framed 1 security-info FnhllAKWRHGAlo+ESXykKAAAAAAAAAAAwAAAAAAAAEaphjojH6pBabDSgSnsfLHeAAAAAgAAAAAAAAAAAAAAAAAAAAEAOQFmCjImkVxP+7sgiYWmMt8FvcOXmlQiTNWFiWlrbpbqgwAAAAAAAATrMIIE5zCCA8+gAwIBAgISBLQp/s3J9CTUIRo+3rnmyeksMA0GCSqGSIb3DQEBCwUAMDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQDEwJSMzAeFw0yMzA5MjAyMDMyMzFaFw0yMzEyMTkyMDMyMzBaMBcxFTATBgNVBAMTDGRvY3Mucm9zLm9yZzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAPHbPDWOsFwNBmcfG3HTYas/KlDjUBXl/UYbJ/SqclEm1LkP8NJVOPEUDCb3+rznIe5NVKMseLynSaiqukTv2LDzZ7RlqUC/W7IcAH7EMl9SdbJklib9qTJn32xiH13dJk6u/m+pwTXk3QLbOa1EY8dE9qIqQzHSiaaHfnBzc/suhZaHDuAamOihqlMUalKeRxWkeIv7HDH926RJ8anRYprVjapK/dlS2aLPnzmqv3uKguszlkBr8ZyH79eXeh8aEYAdLNAMyii4F74vcI/dM6oro93G+v+uSCVg6YYbH0G1mgmtB88cSxfKHSWzp9Gpl1UOjftpY01Ry5B1R6g4xR8CAwEAAaOCAhAwggIMMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUWbIt6P9KDasn49P3HYxDDZFi4+IwHwYDVR0jBBgwFoAUFC6zF7dYVsuuUAlA5h+vnYsUwsYwVQYIKwYBBQUHAQEESTBHMCEGCCsGAQUFBzABhhVodHRwOi8vcjMuby5sZW5jci5vcmcwIgYIKwYBBQUHMAKGFmh0dHA6Ly9yMy5pLmxlbmNyLm9yZy8wFwYDVR0RBBAwDoIMZG9jcy5yb3Mub3JnMBMGA1UdIAQMMAowCAYGZ4EMAQIBMIIBBgYKKwYBBAHWeQIEAgSB9wSB9ADyAHcAtz77JN+cTbp18jnFulj0bF38Qs96nzXEnh0JgSXttJkAAAGKtIJJNgAABAMASDBGAiEA50Bn3IU5TZ6CRtp4Oyx+hZ+hm9BZjf7jrhrnMJ6yswYCIQCJkPmVdBIk1O5lcxiHyjCraZ8xT1Y3gR94ZYqEVu37VgB3AHoyjFTYty22IOo44FIe6YQWcDIThU070ivBOlejUutSAAABirSCSVkAAAQDAEgwRgIhALvembQoS/yMpyGZAQgVxRPwwXiVG5zrE2lug+i7/K/dAiEApKzk4JhikiIjcToqmwok8FEd75rebfuxWdCAMyiLjFQwDQYJKoZIhvcNAQELBQADggEBAJBKpc0XAsDkUEB8j7OJgnvgRbgiyL3t013Rt5T02Ch0uSMNm8uzmj9U8gatiVzIa5vHGkcC79PNiRzil8BiS6YeoCNk9gyhh8XvoOqXj00xjr6K0hvtHhKcOqVgDtayzmMWw6Zs31rwYX8WBZIoZhnCTsIpnCHka9/vD8Yy59IUZiTudtbj9SC45WhPRGsnykbS5vHM34r60QFu3pT3UMUX6U629CPprhrOR85B7R84WIqrkH+5a3E+faP08oRiYojCdsX4MLLk3qV7URWRccc03rlDSy4PR7RSvkOVV7gp+/7PbSJoXdz0+NJJcdbwEqgClscpEV4i8Do+cVT1X9vALwADAAAAAAABAQAAAAAAAARQMjU2AAAAEFJTQS1QS0NTMS1TSEE1MTIAA2YKMiaRXE/7uyCJhaYy3wW9w5eaVCJM1YWJaWtuluqDAAAAAAAABOswggTnMIIDz6ADAgECAhIEtCn+zcn0JNQhGj7euebJ6SwwDQYJKoZIhvcNAQELBQAwMjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxCzAJBgNVBAMTAlIzMB4XDTIzMDkyMDIwMzIzMVoXDTIzMTIxOTIwMzIzMFowFzEVMBMGA1UEAxMMZG9jcy5yb3Mub3JnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8ds8NY6wXA0GZx8bcdNhqz8qUONQFeX9Rhsn9KpyUSbUuQ/w0lU48RQMJvf6vOch7k1Uoyx4vKdJqKq6RO/YsPNntGWpQL9bshwAfsQyX1J1smSWJv2pMmffbGIfXd0mTq7+b6nBNeTdAts5rURjx0T2oipDMdKJpod+cHNz+y6FlocO4BqY6KGqUxRqUp5HFaR4i/scMf3bpEnxqdFimtWNqkr92VLZos+fOaq/e4qC6zOWQGvxnIfv15d6HxoRgB0s0AzKKLgXvi9wj90zqiuj3cb6/65IJWDphhsfQbWaCa0HzxxLF8odJbOn0amXVQ6N+2ljTVHLkHVHqDjFHwIDAQABo4ICEDCCAgwwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBRZsi3o/0oNqyfj0/cdjEMNkWLj4jAfBgNVHSMEGDAWgBQULrMXt1hWy65QCUDmH6+dixTCxjBVBggrBgEFBQcBAQRJMEcwIQYIKwYBBQUHMAGGFWh0dHA6Ly9yMy5vLmxlbmNyLm9yZzAiBggrBgEFBQcwAoYWaHR0cDovL3IzLmkubGVuY3Iub3JnLzAXBgNVHREEEDAOggxkb2NzLnJvcy5vcmcwEwYDVR0gBAwwCjAIBgZngQwBAgEwggEGBgorBgEEAdZ5AgQCBIH3BIH0APIAdwC3Pvsk35xNunXyOcW6WPRsXfxCz3qfNcSeHQmBJe20mQAAAYq0gkk2AAAEAwBIMEYCIQDnQGfchTlNnoJG2ng7LH6Fn6Gb0FmN/uOuGucwnrKzBgIhAImQ+ZV0EiTU7mVzGIfKMKtpnzFPVjeBH3hlioRW7ftWAHcAejKMVNi3LbYg6jjgUh7phBZwMhOFTTvSK8E6V6NS61IAAAGKtIJJWQAABAMASDBGAiEAu96ZtChL/IynIZkBCBXFE/DBeJUbnOsTaW6D6Lv8r90CIQCkrOTgmGKSIiNxOiqbCiTwUR3vmt5t+7FZ0IAzKIuMVDANBgkqhkiG9w0BAQsFAAOCAQEAkEqlzRcCwORQQHyPs4mCe+BFuCLIve3TXdG3lPTYKHS5Iw2by7OaP1TyBq2JXMhrm8caRwLv082JHOKXwGJLph6gI2T2DKGHxe+g6pePTTGOvorSG+0eEpw6pWAO1rLOYxbDpmzfWvBhfxYFkihmGcJOwimcIeRr3+8PxjLn0hRmJO521uP1ILjlaE9EayfKRtLm8czfivrRAW7elPdQxRfpTrb0I+muGs5HzkHtHzhYiquQf7lrcT59o/TyhGJiiMJ2xfgwsuTepXtRFZFxxzTeuUNLLg9HtFK+Q5VXuCn7/s9tImhd3PT40klx1vASqAKWxykRXiLwOj5xVPVf22YKMiaRXE/7uyCJhaYy3wW9w5eaVCJM1YWJaWtuluqDAAAAAAAABRowggUWMIIC/qADAgECAhEAkSsISs8MGKdT9tYuJadfWjANBgkqhkiG9w0BAQsFADBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJuZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBYMTAeFw0yMDA5MDQwMDAwMDBaFw0yNTA5MTUxNjAwMDBaMDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQDEwJSMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALsCFSjM9qCU0w8S7I1VksP4gvGZpnpCiKddJqq1K7nFTLGvjmv5dcij1w9HlBRVNVeMnqiiORn1gjxCqU5u9TvDLtuNwLBc81k45+3PafBaCxu+wJQkJYf6N3GzE+ccrOGb79vkO0VSRZapwVPONMhS7rWu7Y/eYHDipVSrtm0Ol6VANGsr07xm62Y0fPpri49XKZn4MBddunJv+4HFrdKGWD0Xx+cJu/Er94bcwdpxXdRG48ytJcGIvGBndWaz8Rj3olzmU/86iLZHpf8TGOqYCXc/nVP5zwHl9aZwFxSvY6T/mbOTndxTpwb+SIUdoWmuJXW7E8xSA/XtUaGL2xUCAwEAAaOCAQgwggEEMA4GA1UdDwEB/wQEAwIBhjAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwEgYDVR0TAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQUFC6zF7dYVsuuUAlA5h+vnYsUwsYwHwYDVR0jBBgwFoAUebRZ5nu25eQBc4AIiMgaWPbpm24wMgYIKwYBBQUHAQEEJjAkMCIGCCsGAQUFBzAChhZodHRwOi8veDEuaS5sZW5jci5vcmcvMCcGA1UdHwQgMB4wHKAaoBiGFmh0dHA6Ly94MS5jLmxlbmNyLm9yZy8wIgYDVR0gBBswGTAIBgZngQwBAgEwDQYLKwYBBAGC3xMBAQEwDQYJKoZIhvcNAQELBQADggIBAIXKTkc+o/eFRIW81Wd4sphjrXVNHpY9M2VyVC2BoOrD7fggv1/Mt3AAt2479l6U3uQgn6bvi7ID56K1FjyRzrTtOQLnfCWKR+Zlbj9G9NnwzpQr7lTOEryMJ0u4wZgvoq/NcZFKCLfIuCN7BC0I+QhXPoPZBDMKRyF4CYInwyrIm7nOXPJkyMC+ecBPjm1EDF6Suy73ixDh6B1EKdtZIO1juSH4EiaUk1egHWUEwQoirhANQ5ehGB9+4OCGN7Vasb0wv4duKyr/IU4bBcP1GJfwXqzDpbhq8C68OzO57kvezPzkr4QLhj/AVUM29mjhNhdqjpnR/6VApzS3wNBjOTU5dW7yunbIkwLpqUtsF84MAtm9gfuft2jUBmWzgj13U/iOeQOtCjEHdSpD2FWXcsQpDvfEXU7IrkaEMNfyhV8YoXm7515wiwfhhpPDuY/cYXElKq/f7SVQUmiLktzl1rXj2n3Qh2yEITGugvX7uavIiRc94UzlOA72vSu9loEU69XbPSCnflnT4vhY+Vu4SM3+XE8WKf4eVSOvyBGwjep8k5AXL/2soglHRj/w6bC3/yhNaDLWZ14eaaOTuPWdiy8L0lJDpm8yV2VNMoHfOFOFXX5dZinquN3klbXNtVYSQs3ETsYlOERQbezOAFUY/ulJZNROypectFvAc6iruEfCZgoyJpFcT/u7IImFpjLfBb3Dl5pUIkzVhYlpa26W6oMAAAAAAAAFbzCCBWswggNToAMCAQICEQCCEM+w0kDjWURj4LtjgosAMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNVBAYTAlVTMSkwJwYDVQQKEyBJbnRlcm5ldCBTZWN1cml0eSBSZXNlYXJjaCBHcm91cDEVMBMGA1UEAxMMSVNSRyBSb290IFgxMB4XDTE1MDYwNDExMDQzOFoXDTM1MDYwNDExMDQzOFowTzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2VhcmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XCov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpLwYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+DLtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5ysR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZXmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBcSLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2qlPRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TNDTwIDAQABo0IwQDAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUebRZ5nu25eQBc4AIiMgaWPbpm24wDQYJKoZIhvcNAQELBQADggIBAFUfWKm8sqhQ0Ayx2BppICcpCKxhdVyKbviC5Wkv1fZWS7m4cxBZ0yGXfudMcfuy0mCtOagL6hchVoXxUA5Z687gWem6yRXvhp2PhID25OmRkNwXm2IbRfBmldJ8b8LqO+8fz8vWrifxqbDIrv19fpr6IgTr/9l/6pErIrEXDo/yijRbWNj8AclUubgmzIqIM4lMLYQ8gt/ullcFuiy798S3x047gr4xyCJzc5LRwoCkOTkQMyOCTDyfhrJVmB2+KYaMIpue4ms7VzqCcE3cCceJywoHTWzoXY7J786rx7u1K05F1krQJszlcsoIaqWV4xWh96TtySxfpfv/rCgCLr7Xe7vjcXuQFtMHXkZTfDcHQozTxJac1Zm1KuCVGoBIrkw5B87MR6RSlSu6uPut0jNTfeUdTW3VobHHQm/mQCc1XKMotweN540zkOcjn/tQnHlsRtW0FbOWbn6bDJY6uFItP9Zb4fsIwoT+JKijidqsauEYKrGoQ2Fb0x/cO4128i3ojXXfFzNsPVP7e8tBX//cotBhOOGWuKxdizfXddUzwJkRrp1BwXJ1hL4CQUJfZyRIlNGbJ74HP7m4T4F0UeF6t+2dI+K+4NUoBBM8MQOe3Xpsj8YHGMZ/3keOPyieBAbPpVQ0d73siZvpF0PfW9tf/o4eV6LNQJ1+YiLa3hgnAAAAAQAAAAAAAQAAAABGdGxzZmxhZ3MweDAwMDAwMDAwOmRvY3Mucm9zLm9yZzo0NDNecGFydGl0aW9uS2V5PSUyOGh0dHBzJTJDcm9zLm9yZyUyOQAA request-method GET response-head HTTP/1.1 200 OK
Date: Fri, 10 Nov 2023 09:19:40 GMT
Server: Apache
Last-Modified: Thu, 09 Nov 2023 12:01:00 GMT
ETag: "1087-609b6f5188aa8"
Accept-Ranges: bytes
Content-Length: 4231
Content-Type: application/javascript
 original-response-headers Date: Fri, 10 Nov 2023 09:19:40 GMT
Server: Apache
Last-Modified: Thu, 09 Nov 2023 12:01:00 GMT
ETag: "1087-609b6f5188aa8"
Accept-Ranges: bytes
Content-Length: 4231
Keep-Alive: timeout=5, max=98
Connection: Keep-Alive
Content-Type: application/javascript
 ctid 2 uncompressed-len 0   �